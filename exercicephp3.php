<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Tableau des étudiants</title>
        <style>
        body {
            font-size : 13px;
        }
        tr {
            text-align : center;
        }
        </style>
    </head>

    <body>
        <table border="1" width="300" align="center">
            <tr>
                <th> Exercice III</th>
            </tr>
        </table>
        <br>
        <i>1. Créer un tableau « Etudiants » comportant les noms des étudiants.</i><br>
        <table border="1" width="300" align="center">
        <?php
        $etudiants = array('Ali','Kamel','Salwa','Abir', 'Mourad');
        $notes = array(14, 19, 9, 4, 11);
        $rows = sizeof($etudiants);
        ?>
               <tr>
                <th colspan = 2>Liste des Etudiants</th>
            </tr>
            <tr>
                <th>N°.</th>
                <th>Prénom</th>
            </tr>
            <?php
            for ($i=0; $i <= $rows-1 ; $i++)
            {
            ?>
            <tr>
                <td><?php echo $i + 1;?></td>
                <td><?php echo $etudiants[$i];?></td>
            <?php
            }
            ?>
            </tr>
        </table>
        <br>
        <i>2. Créer un tableau « Notes » comportant les notes.</i><br>
        <table border="1" width="300" align="center">
            <tr>
                <th colspan = 2> Notes DS</th>
            </tr>
            <tr>
                <th>N°.</th>
                <th>Note</th>
            </tr>
            <?php
            for ($i=0; $i <= $rows -1 ; $i++)
            {
            ?>
            <tr>
                <td><?php echo $i+1;?></td>
                <td><?php echo $notes[$i];?></td>
            <?php
            }
            ?>
            </tr>
        </table>
<br>
        <i>3. Afficher l’étudiant qui a la meilleure note.</i><br>
        <i>4. Afficher l’étudiant qui a la mauvaise note.</i><br>
        <i>5. Afficher la moyenne des notes.</i><br>
        <i>6. Si la moyenne est supérieure à 15 alors afficher « Oui » sinon afficher « Non ».</i><br>
        <table border="1" width="300" align="center">
            <tr>
                <th colspan = 2>Statistiques</th>
            </tr>
            <tr>
                <th>Meilleure Note</th>
                <?php
                $max = max ($notes);
                $position = array_search($max, $notes);
                ?>
                <td><?php echo $etudiants[$position];?></td>
            </tr>
            <tr>
                <th>Mauvaise Note</th>
                <?php
                $min = min ($notes);
                $position = array_search($min, $notes);
                ?>
                <td><?php echo $etudiants[$position];?></td>
            </tr>
            <tr>
                <th>Moyenne Générale</th>
                <?php
                $somme_notes = 0;
                $i = 0;
                foreach($notes as $cle => $valeur)
                {
                    $i++; 
                    $somme_notes+=$valeur;
                }
                $moyenne = $somme_notes/$i;
                ?>
                <td><?php echo $moyenne;?></td>
            </tr>
            <tr>
                <th>Evaluation</th>
                <?php
                if ($moyenne >= 15)
                {
                    $evaluation = "Oui";

                }
                else
                {
                    $evaluation = "Non";
                }
                ?>
                <td><?php echo $evaluation;?></td>
            </tr>
        </table>
<br>
        <i>7. Afficher les étudiants et leurs notes resp.</i><br>
        <table border="1" width="300" align="center">
        <tr>
                <th>Etudiant</th>
                <th>Note</th>
            </tr>
            <?php
            for ($i=0; $i <= $rows -1 ; $i++)
            {
            ?>
            <tr>
                <td><?php echo $etudiants[$i];?></td>
                <td><?php echo $notes[$i];?></td>
            <?php
            }
            ?>
            </tr>
        </table>
        ■
    </body>
</html>
